# Programación concurrente 

## Paradigma

Es común que se crea que creamos que nos referimos a *Simultaneidad* (Programas ejecutados en el mismo instante y lugar (imposible))

Realmente cuando hablamos de Concurrencia nos referimos a los siguientes paradigmas:

 - **Paralelismo**: Programas ejecutados en el mismo intervalo de tiempo (Varios núcleos)
 - **Solapamiento**: Programas superpuestos

 Aunque fundamentalmente se trata de **Solapamiento** que nos dará la falsa ilusión de *Simultaneidad*
 
## Relaciones
Las principales relaciones entre procesos son:
 - **Independencia**: Los procesos no tienen relación ya sea por ejecutarse en distintos equipos o por no coincidir en el mismo periodo de tiempo.
 - **Competencia**: Distintos procesos necesitan de los mismos recursos (Memoria, CPU...).
 - **Cooperación**: Los procesos se necesitan y colaboran para una solución común.
 
## Interacción
Los procesos pueden interactuar entre si para  de las siguientes maneras:
 - **Sincronización**: Se intercambia información del *flujo de ejecución*.
    * **Sincronización condicional**: Un proceso espera la ejecución del otro para el acceso a un recurso. (Puede provocar interbloqueo *pasivo*)
    * **Exclusión Mutua**: Un proceso bloquea al otro el acceso a un recurso. (Puede provocar interbloqueo *activo*)
 - **Comunicación**: Se intercambia información de los *datos*.
 
## Estados de un proceso

![./mermaid-diagram.svg](./mermaid-diagram.svg)
## Paralelismo secuencial
![./mermaid-diagram.svg](./mermaid-diagram-2.svg)

## Paralelismo real 
![./mermaid-diagram.svg](./mermaid-diagram-3.svg)

## Paralelismo simulado o Solapamiento
![./mermaid-diagram.svg](./mermaid-diagram-4.svg)

## SSTE
El **Sistema de Soporte en Tiempo de Ejecución** es el encargado de:
- **Planificación:** Asignar instante a instante cada procesador a un proceso de forma justa.
- **Despacho:** Asegurar la toma de la CPU al proceso elegido.
~~~
El comportamiento funcional de un programa no debe depender del algoritmo de planificación.
~~~

~~~
Al cambiar entre procesos se guarda el valor de todos los registros de la UCP para poder restaurarlo en las mismas condiciones de ejecución.

~~~
    
El SSTE se activa en las siguientes casuísticas:
- Finalización de un proceso.
- Bloqueo de un proceso.
- Finalización de la cuota de tiempo.
- Ocurre un error de ejecución.
- Un proceso mas prioritario pasa a preparado.
    
## Instrucciones atómicas
Decimos que una instrucción es atómica cuando su ejecución es indivisible
Las hay de dos tipos:
- **De grano fino**: Hardware
- **De grano grueso**: Software
    
## Propiedades de un sistema
Un sistema debe cumplir ciertas propiedades para ser estable.

Las **propiedades de seguridad** son aquellas que se deben cumplir **en todo momento** y son las siguientes:
- **Exclusión mutua**: evitar varios proceso simultáneos en su sección critica.
- **Ausencia de interbloqueo pasivo**(deadlock): en ningún momento dos procesos deben esperarse mutuamente inutilizándose ambos en un bucle infinito.
Las **propiedades de vida** son aquellas que se deben cumplir **eventualmente** y son las siguientes:
- **Ausencia de interbloqueo activo (livelock)**:En algún momento dos procesos deben dejar de bloquear un mismo recurso para que uno de ellos acceda.
- **Propiedades de Justicia:** Una petición debe ser eventualmente atendida.
    
## Tipos de justicia
según el tipo de petición que implemente diremos que se trata de justicia fuerte o débil
- **Justicia Débil:** Petición Continua.
- **Justicia Fuerte:** Petición Infinitamente frecuente.

## Propiedades de justicia
Para que exista justicia se han de cumplir las siguientes propiedades con cada petición:
- **Ausencia de inanición:** debe garantizarse que sea atendida eventualmente.
- **Espera Lineal:** debe garantizarse que se le atienda antes de que a otro proceso se le conceda dos veces.
- **Espera FIFO:** debe atenderse antes que a cualquier otro proceso que la haya pedido posteriormente.
