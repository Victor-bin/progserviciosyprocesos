#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

sig_atomic_t count_usr2 = 0;

void handler (int signal){
    count_usr2++;
}

int main (int argc, char** argv)
{
    struct sigaction signal_action_handler;
    memset(&signal_action_handler, 0, sizeof(signal_action_handler));
    signal_action_handler.sa_handler = &handler;
    sigaction(SIGUSR2, &signal_action_handler, NULL);

    while ( count_usr2 < 2 )
    sleep(10000);

    return  EXIT_SUCCESS;
}
