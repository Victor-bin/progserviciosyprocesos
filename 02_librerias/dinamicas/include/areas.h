#ifndef __AREAS_H__
#define __AREAS_H__

#ifdef __cplusplus
extern "C"
{
#endif
    int area_cuadrado (int lado);
    int area_rectangulo (int base, int altura);
    int area_triangulo (int base, int altura);
#ifdef __cplusplus
}
#endif

#endif

