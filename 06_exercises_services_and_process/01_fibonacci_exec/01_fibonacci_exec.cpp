#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

// @author Victor-bin / VictorTi
// @date 28 - 10 - 2020 (dd - mmd - yyyy)

int fibonacii (char* program_name, int num1, int num2, int stop_condition){
    int result = num1 + num2;
    char arg1[10];
    char arg2[10];
    char arg3[10];
    pid_t child_pid;

    sprintf(arg1, "%d", num2);
    sprintf(arg2, "%d", result);
    sprintf(arg3, "%d", stop_condition);

    char* arg_list[] = {program_name, arg1, arg2, arg3, NULL};

    if(result < stop_condition){
        child_pid = fork();
        if (child_pid != 0)
            execvp(program_name, arg_list);
    }
    return result;
}

int main (int argc, char* argv [])
{
    int result = 0;
    if (argc > 3) {
        result = fibonacii(argv[0], atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));
        wait(NULL);
        printf("%i\n", result);
    }
    else
    printf("faltan argumentos.\n");

    return EXIT_SUCCESS;
}

