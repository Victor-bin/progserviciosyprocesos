#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "pila.h"

sig_atomic_t count_usr1 = 0;
sig_atomic_t child_loop = 1;
sig_atomic_t file_id;
sig_atomic_t numbers_writed = 10;

void handler (int signal){
    switch (signal){
        case SIGUSR1:
            count_usr1++;
            break;
        case SIGINT:
            printf(" No esta permitido el uso de Ctr C \n ");
            break;
        case SIGTERM:
            child_loop = 0;
            break;
    }
}

int write_temp_file (int* buffer, size_t length){
    char temp_filename[] = "/tmp/temp_file.XXXXXX";
    int fd = mkstemp (temp_filename);
    write (fd, &length, sizeof (length));
    write (fd, buffer, length);
    return fd;
}

int* read_temp_file (int temp_file, int length){
    int* buffer;
    int fd = temp_file;
    lseek (fd, 0, SEEK_SET);
    buffer = (int*) malloc (length * sizeof(int));
    read (fd, buffer, length);
    lseek (fd, 0, SEEK_SET);
    close (fd);
    return buffer;
}

pid_t primo(){
    struct TPila pila;
    inicializar(&pila);
    pid_t child_pid;
    child_pid = fork();
    int nums = 0;
    if (child_pid == 0){
        int divisores;
        for(int i = 2; child_loop; i++, divisores = 0){
            for(int n = 2; n < (i / 2); n++)
                if (i % n == 0)
                    divisores++;
            if (!divisores)
                guardar(&pila,  i);
        }
        file_id = write_temp_file(pila.datos, pila.cima);
        numbers_writed = pila.cima - 1;
        nums = pila.cima - 1;
        desalojar(&pila);
        return numbers_writed;
    }
}



int main (int argc, char** argv){
    struct sigaction signal_action_handler;
    memset(&signal_action_handler, 0, sizeof(signal_action_handler));

    signal_action_handler.sa_handler = &handler;
    sigaction(SIGUSR1, &signal_action_handler, NULL);
    sigaction(SIGTERM, &signal_action_handler, NULL);
    sigaction(SIGINT, &signal_action_handler, NULL);

    pid_t primos = primo();

    if (primos != 0){
        wait(NULL);
        int length = numbers_writed;
        int* result = (int*) malloc( length * sizeof(int) );
        result = read_temp_file (file_id, length);
        for (int i = 0; i < length/10; i++)
            printf("%i es primo\n",result[i]);
        free(result);
        printf("Se ha llamado a SIGUSR1 %i veces\n", count_usr1);
    }
    return  EXIT_SUCCESS;
}
