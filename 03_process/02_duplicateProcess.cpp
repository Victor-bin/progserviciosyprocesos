#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main (int argc, char** argv)
{
    pid_t child_pid;
    printf("The process ID is %d\n", (int) getpid());
    child_pid = fork();
    if(child_pid){
        printf("The parent ID is %d\n", (int) getpid());
        printf("The child ID is %d\n", (int) child_pid);
    } else
        printf("The child process ID is %d\n", (int) getpid());

    return EXIT_SUCCESS;
}
