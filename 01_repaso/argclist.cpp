#include <stdio.h>
#include <stdlib.h>

int main (int argc, char * argv [])
{
    printf("El nombre de este programa es '%s'. \n", argv [0]);
    printf("Este programa fue invocado con %d argumentos. \n", argc - 1);

    /* ¿Se especificó algún argumento en la línea de comandos? */

    if (argc> 1) {
        /* Sí, imprímalos. */
    int i;
    printf("Los argumentos son: \n");
    for (i = 1; i < argc; ++i)
        printf("%s\n", argv[i]);
    }
    return EXIT_SUCCESS;
}

