#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

// @author Victor-bin / VictorTi
// @date 29 - 10 - 2020 (dd - mmd - yyyy)

int fibonacii (int num1, int num2, int stop_condition){
    int result = num1 + num2;
    pid_t child_pid;

    if(result < stop_condition){
        child_pid = fork();
        if (child_pid != 0)
            result = fibonacii(num2, result, stop_condition);
    }
    return result;
}

int main (int argc, char* argv [])
{
    int result = 0;
    if (argc > 3) {
        result = fibonacii(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));
        wait(NULL);
        printf("%i\n", result);
    }
    else
    printf("faltan argumentos.\n");

    return EXIT_SUCCESS;
}

