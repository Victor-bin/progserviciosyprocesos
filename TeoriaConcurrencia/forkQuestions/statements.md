los enunciados se encuentran en https://gitlab.com/txemagon/services-and-processes/-/blob/master/history/202021-apocalipsis/enunciados/procesos.md

# Pregunta 1
## Salida Pregunta 1
```bash
Hello world!
Hello world!
```
## Diagrama de procesos Pregunta 1
![./fork1.svg](./fork1.svg)

# Pregunta 2
## Salida Pregunta 2
```bash
hello
hello
hello
hello
hello
hello
hello
hello
```
## Diagrama de procesos Pregunta 2
![./fork2.svg](./fork2.svg)


# Pregunta 3
## Salida Pregunta 3
```bash
Hello from Parent!
Hello from Child!
```

# Pregunta 4
## Salida Pregunta 4
```bash
Parent has x = 0
Child has x = 2
```

# Pregunta 5
## Respuesta Pregunta 5
```math
2^n
```

# Pregunta 6
## Salida Pregunta 6
```bash
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
```
## Diagrama de procesos Pregunta 6
![./fork6.svg](./fork6.svg)
