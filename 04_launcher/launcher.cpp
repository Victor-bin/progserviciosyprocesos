#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

sig_atomic_t count_programs = 0;
sig_atomic_t count_sigchild = 0;

void handler (int signal){
    count_sigchild++;
}

int spawn (char* program, char** arg_list)
{
    pid_t child_pid;
    child_pid = fork ();
    if (child_pid != 0)
        return child_pid;
    else {
        execvp (program, arg_list);
        fprintf (stderr, "an error occurred in execvp\n");
        abort ();
    }
}


int main (int argc, char** argv)
{
    struct sigaction sig_act_handler;
    memset(&sig_act_handler, 0, sizeof(sig_act_handler));
    sig_act_handler.sa_handler = &handler;
    sigaction(SIGCHLD, &sig_act_handler, NULL);
    char* arg_list[] = {".",NULL};
    char* program_list[] = {"okular","firefox","dolphin"};

    char* menu ="__ __ __ LAUNCHER MENU __ __ __\n\
                     .1  Okular\n\
                     .2  Firefox\n\
                     .3  Dolphin\n\
                     .4  EXIT\n";

    while(true){
        printf(menu);
        printf("\n INTRODUCE OPTION: ");
        fflush(stdin);
        int i;
        scanf("%i",&i);
        switch(i){
            case 1:
                spawn (program_list[i-1], arg_list);
                count_programs++;
                break;
            case 2:
                spawn (program_list[i-1], arg_list);
                count_programs++;
                break;
            case 3:
                spawn (program_list[i-1], arg_list);
                count_programs++;
                break;
            case 4:
                while (count_sigchild < count_programs)
                    wait(NULL);
                return EXIT_SUCCESS;
            default:
                printf(" Valor invalido");
                break;
        }
    }
    return  EXIT_FAILURE;
}
