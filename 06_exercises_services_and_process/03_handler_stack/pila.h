#ifndef __PILA_H_
#define __PILA_H_

#include <stdlib.h>

#define BLCSZ 8

struct TPila {
    int *datos;
    int cima;
    unsigned tamanyo;
};

#ifdef __cplusplus
extern "C"
{
#endif
    void inicializar (struct TPila *p);
    void alojar (struct TPila *p);
    void desalojar (struct TPila *p);
    void guardar (struct TPila *p, int dato);
    int    sacar (struct TPila *p);
#ifdef __cplusplus
}
#endif

#endif
