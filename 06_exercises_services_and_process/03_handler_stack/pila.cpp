#include "pila.h"

void alojar (struct TPila *p){
    p -> datos = (int *) realloc(p -> datos,(p -> tamanyo + BLCSZ) * sizeof(int));
    p -> tamanyo += BLCSZ;
}

void inicializar (struct TPila *p){
    p -> cima = 0;
    p -> tamanyo = 0;
    p -> datos = NULL;
    alojar(p);
}

void desalojar (struct TPila *p){
    free(p -> datos);
}

void guardar (struct TPila *p, int dato){
    while(p -> cima >= p-> tamanyo)
        alojar(p);
    p -> datos[p -> cima++] = dato;
}

int sacar (struct TPila *p){
    if(p -> cima <= 0)
        return -404;
    return p -> datos[--p -> cima];
}
